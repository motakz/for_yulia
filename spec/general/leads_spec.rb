require 'spec_helper'

describe "Leads" do

  before(:all) do
    login_to_autotest
  end

  it "creates a new lead" do |lead_name|
    visit(NewLeadPage)
    on(NewLeadPage).last_element.when_visible(10).click
    on(NewLeadPage).last_element.when_visible(10).value = lead_name
    on(NewLeadPage).save_element.click
  end

  it "opens the lead, adds a note and checks the note in the feed" do |note_body|
    on(LeadDetailPage).note_body_element.when_visible(10).value = note_body
    on(LeadDetailPage).save_element.click
    expect(@current_page.note_bodies).to include note_body

  end

  end
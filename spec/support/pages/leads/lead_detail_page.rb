class LeadDetailPage < AbstractPage
  include PageObject

  text_area(:note_body, :class => "span12", :name => "note")
  button(:save, :class => "btn btn-inverse hide", :text => "Save")
  unordered_list(:feed_items, :css => ".feed-items")

  def note_bodies
    bodies = []
    feed_items_element.when_visible.list_item_elements.each do |item|
      bodies << item.div_element(:class => "item-body").paragraph_element(:class => "activity-content note-content").div_element.text
    end
    bodies
  end

end
class NewLeadPage < AbstractPage
  include PageObject

  page_url "https://app.futuresimple.com/leads/new"
  
  text_field(:last, :id => 'lead-last-name')
  button(:save, :css => '.save.btn')

  end